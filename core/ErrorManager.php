<?php 

class ErrorManager extends Exception 
{
	private $_error = array();
	private $_email = 'jmorgan@affinitysolutions.com';
	private $_redirectURL;
	private $_logName = "PHP-NewCode";
	private $_trace;
	private $_log_trace;	
	private $_cli;

	function __construct($e)
	{
		$this->_cli = substr(php_sapi_name(), 0, 3) == 'cli';
		$this->_log_trace = $this->getLogErrorTrace();
		$this->_error['message'] = $e;
		$this->_error['details'] = $this->_log_trace."\n\n".print_r($_SERVER, true);
	}

	function setEmail($email)
	{
		$this->_email = $email;
	}
	
	function setRedirectURL($url)
	{
		$this->_redirectURL = $url;
	}
	
 	function getError()
 	{
		return $this->_error;
	}

	function logError($type)
	{
		$log_message = "Message: {$this->_error['message']} Trace: {$this->_log_trace}";
		$syslog_facility = defined('LOG_LOCAL5') ? LOG_LOCAL5 : LOG_USER;
		openlog($this->_logName, LOG_PID | LOG_PERROR, $syslog_facility);
		syslog($type, $log_message);
		closelog();
	}
	
	function getLogErrorTrace()
	{
		$trace = debug_backtrace();
		$trace_parts = array();
		foreach($trace as $index => $trace_data)
		{
			$file = str_replace('/usr/local/apache/','',$trace_data['file']);
			$trace_parts[]= "$file:{$trace_data['line']}" . (isset($trace_data['function']) ? ":{$trace_data['function']}" : '');
		}

		$break = $this->_cli ? "\n" : "<br />";
		return implode($break, $trace_parts);
	}
	
	function handleError($exit=true, $send=true, $print=true, $type=null)
	{
		$type = is_null($type) ? ($exit == true ? LOG_ERR : LOG_WARNING) : $type;
		$this->logError($type);
		$input = new Input();
		$debug = $input->request('debug');
		
		if (((defined('DEBUG') && DEBUG) || $debug == 'true') && $exit)
		{
			if($print && $this->_redirectURL=='')
			{
				echo '<pre>Message: '.$this->_error['message'].'<br>Details: '.$this->_error['details'];
			}
			else 
			{
				$this->_error['details'] = str_replace('<br/>', "\n", $this->_error['details']);
				Email::sendAlert($this->_email, 'SITE DOWN ALERT: '.$_SERVER['HTTP_HOST'].' '.substr($this->_error['message'], 0, 100), print_r($this->_error, true));
			}
		}
		else
		{
			if ($send)
			{
				$this->_error['details'] = str_replace('<br/>', "\n", $this->_error['details']);
				Email::sendAlert($this->_email, 'SITE DOWN ALERT: '.$_SERVER['HTTP_HOST'].' '.substr($this->_error['message'], 0, 100), print_r($this->_error, true));
			}
			if (Page::isAPI())
			{
				if ($exit)
				{
					$output = array('status' => 'fail', 'error' => $this->_error['message']);
					echo json_encode($output);
					exit;
				}
			}
			elseif ($this->_cli)
			{
				echo $this->_error['message'];
			}
			else
			{
				if ($exit)
				{
					if ($this->_redirectURL!='')
					{
						HTTP::redirect("{$this->_redirectURL}");
					}
					else
					{
						HTTP::redirect(WWWPATH.'/oops.php');
					}
				}
			}
		}
		if ($exit && $this->_redirectURL=='') exit;
	}
}
?>