<?php

class Loader
{
	function __call($type, $arguments)
	{
		$class = isset($arguments[0]) ? $arguments[0] : '';
		$params = isset($arguments[1]) ? $arguments[1] : null;
		$name = isset($arguments[2]) ? $arguments[2] : null;
		$this->loadClass($type, $class, $params, $name);
	}
	
	private function loadClass($type, $class, $params = null, $name = null)
	{
		$Controller = Controller::getInstance();
		$model = $name == 'model' ? $class . 'Model' : $class;
		$classlower = strtolower($class);
		$newclass = !is_null($name) ? $name : $classlower;

		if (!property_exists($Controller, $classlower))
		{
			if (!class_exists($model))
			{
				$model = $class;
				if ($type == 'library')
				{
					if (is_file(SYSPATH . "/library/$model.php"))
					{
						include_once SYSPATH . "/library/$model.php";
					}
					elseif (is_file(APPPATH . "/library/$model.php"))
					{
						include_once APPPATH . "/library/$model.php";
					}
					else
					{
						echo "Unknown load library: $model";
					}
				}
				elseif ($type == 'model')
				{
					if (is_file(APPPATH . "/library/$model.php"))
					{
						include_once APPPATH . "/library/$model.php";
					}
					else
					{
						return;
					}
				}
				elseif ($type == 'helper')
				{
					require_once SYSPATH . "/helper/$model.php";
				}
				else
				{
					echo "Unknown load method: $type";
					exit;
				}
			}
			$Controller->$newclass = new $model($params);
		}
	}

	function view($template, $data = null)
	{
		$Controller = Controller::getInstance();

		ob_start();
		$objects = (array_keys(get_object_vars($Controller)));
		unset($objects['load'], $objects['view']);

		foreach ($objects as $o)
		{
			$this->$o = $Controller->$o;
		}

		if (file_exists(APPPATH . "/view/$template.php"))
		{
			include APPPATH . "/view/$template.php";
		}
        elseif (file_exists(APPPATH."/view/$template"))
		{
			include APPPATH."/view/$template";
		}
		else
		{
			echo "No view setup for $template";
			exit;
		}
		$Controller->view->setOutput(ob_get_contents());
		@ob_end_clean();
	}

}
