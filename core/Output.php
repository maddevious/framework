<?php 
//test
class Output
{
	protected $output;	
	protected $data = array();
	protected $Controller;
	protected $json = false;
	protected $tokenReplace = true;
	protected $input;
	protected $replaceObjects = array();
	
	function __construct()
	{
		$this->Controller = Controller::getInstance();
	}
	
	function setTokenReplaceObjects($objects)
	{
		foreach ($objects as $o)
		{
			$this->replaceObjects[] = $this->Controller->$o;
		}
	}
	
	function mergeTokenReplaceObjects()
	{
		$arrayObjects = array();
		if (!empty($this->replaceObjects))
		{
			foreach ($this->replaceObjects as $o)
			{
				$arrayObjects = array_merge($arrayObjects, Arrays::extractNonArrays($o));
			}
			$this->setData($arrayObjects);
		}
	}
	
	function disableTokenReplace()
	{
		$this->tokenReplace = false;
	}
	
	function enableTokenReplace()
	{
		$this->tokenReplace = true;
		$this->Controller = Controller::getInstance();
	}

	function setOutput($data)
	{
		if (is_array($data))
		{
			$this->output = Arrays::json_encode($data);
			$this->json = true;
		}
		else
		{
			$this->output .= $data;
		}
	}
	
	function setData($data)
	{
		if (is_array($data))
		{
			$this->data = array_merge($this->data, $data);
		}
	}
	
	function display($content_type = 'text/html', $return = false)
	{
		if ($this->tokenReplace)
		{
			$this->tokenReplace();
		}
		
		if ($this->json)
		{
			if ($content_type == 'text/html')
			{
				return Arrays::json_decode($this->output, true);
			}
			else
			{
				$this->output = Arrays::json_decode($this->output);
			}
		}

		if ($return)
		{
			return $this->output;
		}
		else
		{
			if ($content_type == 'text/json' || $content_type == 'application/json')
			{
				$content_type == 'text/json' ? header("Content-type: text/html") : header("Content-type: application/json");
				$output = Arrays::json_encode($this->output);
				$output = utf8_decode($output);
				echo $output;
			}
			elseif ($content_type == 'array')
			{
				header("Content-type: text/html");
				$output = Arrays::json_encode($this->output);
				$output = utf8_decode($output);
				dumpArray(Arrays::json_decode($output, true));
			}
			elseif ($content_type == 'text/css')
			{
				header("Content-type: ".$content_type);
				echo $this->output;
			}
			else
			{
				header("Content-type: ".$content_type);
				header("Expires: -1");
				header("Pragma: no-cache");
				header("Cache-Control: no-cache");
				header("Cache-Control: no-store");
				echo utf8_decode($this->output);
			}
			if ($content_type == 'text/html' && isset($this->Controller->profiler->status) && $this->Controller->profiler->status == 'enable')
			{
				dumpArray($this->Controller->profiler->output());
			}
		}
		exit;
	}
	
	function tokenReplace()
	{
		$input = new Input();
		$this->mergeTokenReplaceObjects();
		$tokens = $this->extractTokens();
		$replace = array();
		
		foreach ($tokens as $t)
		{
			$t = trim($t);
			if ($t != '')
			{
				$tt = trim(strtolower($t), '|');
				$replace[] = isset($this->data[$tt]) ? $this->data[$tt] : $t;
			}
		}

		$defines = get_defined_constants(true);
		$user_defines = isset($defines['user']) ? $defines['user'] : array();
		foreach ($user_defines as $k => $v)
		{
			$user_defines['|'.$k.'|'] = $v;
			unset($user_defines[$k]);
		}

		$tokens = array_merge($tokens, array_keys($user_defines));
		$replace = array_merge($replace, array_values($user_defines));
		//dumpArray($tokens); dumpArray($replace); exit;
		$this->output = str_replace($tokens, $replace, $this->output);
	}
	
	function extractTokens()
	{
		$unique = array();
		preg_match_all('/\|[A-Z0-9]*?\|/', $this->output, $matches1);
		preg_match_all('/\|CURR:\s*[A-Z0-9$%]*?\|/', $this->output, $matches2);
		$matches = array_merge($matches1[0], $matches2[0]);

		if (count($matches) > 0)
		{
			$unique = Arrays::arrayUnique($matches);
		}
		return $unique;
	}
}
