<?php

class Controller
{

    public $controller;
    protected $method;
    private static $instance;

    function __construct($controller = '', $method = '')
    {
        if (get_called_class() == 'Controller')
        {
            self::$instance = $this;
            $this->controller = $controller != '' ? $controller : 'Home';
            $this->method = $method != '' ? $method : 'index';
            $this->input = new Input();
            $this->load = new Loader();
            $this->view = new Output();
            @include_once APPPATH . "/config/autoload.php";
        }
        else
        {
            $Controller = Controller::getInstance();
            $objects = array_flip(array_keys(get_object_vars($Controller)));
            unset($objects['controller'], $objects['method']);
            $objects = array_flip($objects);

            foreach ($objects as $o)
            {
                $this->$o = $Controller->$o;
            }
            self::$instance = $this;
        }
    }

    public static function getInstance()
    {
        return self::$instance;
    }

    function invoke()
    {
        $class = $this->controller;
        $controller = $this->controller . 'Controller';

        include_once APPPATH . "/controller/$controller.php";

        if (class_exists($controller))
        {
            $ctrl = new $controller();
            $ctrl->controller = $class;
            $ctrl->load->model($class);

            if (method_exists($ctrl, $this->method))
            {
                $ctrl->{$this->method}();
            }
            else
            {
                $ctrl->index();
            }
        }
        else
        {
            echo "Controller not supported: $controller";
            exit;
        }
    }

    function getErrorMessages($file)
    {
        $errids = $this->input->request('errid') != '' ? str_split($this->input->request('errid')) : array();
        $output = array();

        if (empty($errids))
        {
            return $output;
        }
        $errors = Cache::loadError($file, false);
        foreach ($errids as $errid)
        {
            $output[$errid]['message'] = isset($errors[$errid]['message']) ? $errors[$errid]['message'] : '';
            $output[$errid]['type'] = isset($errors[$errid]['type']) ? $errors[$errid]['type'] : 'error';
        }
        return $output;
    }
}
?>