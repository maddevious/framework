<?php

class Input
{

	function get($var = null, $type = 'string')
	{
		if ($var === '')
			return '';
		$this->process($_GET, $var, $type);
		if (!is_null($var) && !strstr($var, '*'))
		{
			return $this->$var;
		}
		else
		{
			return $this;
		}
	}

	function post($var = null, $type = 'string')
	{
		if ($var === '')
			return '';
		$this->process($_POST, $var, $type);
		if (!is_null($var) && !strstr($var, '*'))
		{
			return $this->$var;
		}
		else
		{
			return $this;
		}
	}

	function cookie($var = null, $type = 'string')
	{
		if ($var === '')
			return '';
		$this->process($_COOKIE, $var, $type);
		if (!is_null($var))
		{
			return $this->$var;
		}
		else
		{
			return $this;
		}
	}

	function request($var = null, $type = 'string')
	{
		if ($var === '')
			return '';
		$this->process($_REQUEST, $var, $type);
		if (!is_null($var) && !strstr($var, '*'))
		{
			return $this->$var;
		}
		else
		{
			return $this;
		}
	}

	function process($superglobal, $var, $type = 'string')
	{
		if (!is_null($var))
		{
			if (strstr($var, '*'))
			{
				$var = str_replace('*', '', $var);
				foreach ($superglobal as $k => $v)
				{
					if (preg_match("/$var.*/", $k))
					{
						$this->$k = Security::xssClean($v, $type);
					}
				}
			}
			else
			{
				if ($var != '')
				{
					$superglobal[$var] = isset($superglobal[$var]) ? $superglobal[$var] : '';
					$this->$var = Security::xssClean($superglobal[$var], $type);
				}
			}
		}
		else
		{
			foreach ($superglobal as $k => $v)
			{
				$this->$k = Security::xssClean($v, $type);
			}
		}
	}

}
