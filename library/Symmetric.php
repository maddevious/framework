<?php 

class Symmetric
{
	protected $key;
	protected $iv;
	protected $mode;
	protected $passphrase;
	protected $cipher;
	protected $timeDiffMax;
	protected $errors = array();
	
	function setEncryptionInfo($key, $iv, $passphrase, $mode = MCRYPT_MODE_CBC, $cipher = MCRYPT_RIJNDAEL_128, $timeDiffMax = 180)
	{
		$this->key = $key;
		$this->iv = $iv;
		$this->mode = $mode;
		$this->passphrase = $passphrase;
		$this->cipher = $cipher;
		$this->timeDiffMax = $timeDiffMax;
		
		if (strlen($this->key) != 32)
		{
			$e = new ErrorManager("Key must be 32 characters");
			$e->handleError();
		}
		if (strlen($this->iv) != 16)
		{
			$e = new ErrorManager("IV must be 16 characters");
			$e->handleError();
		}
	}
	
	function encrypt($data)
	{
		if (!isset($data['passphrase']) || $data['passphrase'] == '')
		{
			$this->errors[] = "No passphrase specified";
			return false;
		}
		date_default_timezone_set("UTC");
		$curtime = time();
		date_default_timezone_set('America/New_York');
		$data['timestamp'] = isset($data['timestamp']) ? $data['timestamp'] : $curtime;
		$ciphertext = mcrypt_encrypt($this->cipher, $this->key, Arrays::json_encode($data), $this->mode, $this->iv);
		return base64_encode($data);
	}
	
	function decrypt($token)
	{
		$token = str_replace(" ", "+", $token);
		date_default_timezone_set("UTC");
		$curtime = time();
		date_default_timezone_set('America/New_York');
		$decrypted_json = mcrypt_decrypt($this->cipher, $this->key, base64_decode($token), $this->mode, $this->iv);
	
		if (empty($decrypted_json))
		{
			$this->errors[] = "Unable to decrypt data";
			return false;
		}
		$decrypted = Arrays::json_decode($decrypted_json);
		
		if (empty($decrypted_json))
		{
			$this->errors[] = "Unable to decode json data";
			return false;
		}
		if (!isset($decrypted['passphrase']) || $decrypted['passphrase'] != $this->passphrase)
		{
			$this->errors[] = "Invalid passphrase: ".$decrypted['passphrase'];
			return false;
		}
		if (abs($decrypted['timestamp'] - $curtime) > $this->timeDiffMax)
		{
			$this->errors[] = 'Timestamp failure: '.$decrypted['timestamp'].' vs '.$curtime.'='.abs($decrypted['timestamp'] - $curtime);
			return false;
		}
		return $decrypted;
	}
	
	function getErrors()
	{
		return $this->errors;
	}
}