<?php

class FormValidation
{

	protected $validation = array();
	protected $input;
	protected $errid;

	function __construct()
	{
		$this->input = new Input();
	}

	function get($key = null)
	{
		if (!empty($key))
		{
			return $this->validation[$key];
		}
		else
		{
			return $this->validation;
		}
	}

	function set($key, $rule, $default = '')
	{
		$this->validation[$key] = $rule;

		// set REQUEST to pre-populate form with information from member object
		if ($this->input->post('formsubmit') == '')
		{
			$_REQUEST[$key] = $default != '' ? $default : '';
		}
	}

	function delete($key)
	{
		if (is_array($key))
		{
			foreach ($key as $k)
			{
				unset($_REQUEST[$k], $_POST[$k], $this->validation[$k]);
			}
		}
		else
		{
			unset($_REQUEST[$key], $_POST[$key], $this->validation[$key]);
		}
	}

	function execute()
	{
		$Controller = Controller::getInstance();
		$cleanpost = $this->input->post();

		foreach ($this->validation as $k => $rules)
		{
			$cleanpost->$k = isset($cleanpost->$k) ? $cleanpost->$k : '';
			if (!empty($this->errid))
			{
				break;
			}
			foreach ($rules as $rule => $errid)
			{
				if ($rule == 'ignore')
				{
					continue;
				}
				elseif ($rule == 'required')
				{
					if ($cleanpost->$k == '' && $errid != '')
					{
						$this->errid = $errid;
						break;
					}
				}
				elseif (preg_match('/match\[.*?\]/', $rule, $matches))
				{
					$matchfield = str_replace(array('match[', ']'), array('', ''), $matches[0]);
					if (!isset($cleanpost->$matchfield))
					{
						$e = new ErrorManager("Unknown match field specific: $matchfield");
						$e->handleError();
					}

					if ($cleanpost->$k != $cleanpost->$matchfield && $errid != '')
					{
						$this->errid = $errid;
						break;
					}
				}
				elseif (function_exists($rule))
				{
					if (!$rule($cleanpost->$k) && $errid != '')
					{
						$this->errid = $errid;
						break;
					}
				}
				elseif (method_exists($Controller, $rule))
				{
					if (!$Controller->$rule($cleanpost->$k) && $errid != '')
					{
						$this->errid = $errid;
						break;
					}
				}
				else
				{
					$e = new ErrorManager("Unknown validation specified: $rule for $k");
					$e->handleError();
				}
			}
		}
		return empty($this->errid) ? true : false;
	}

	function setErrors($errid)
	{
		$this->errid = $this->errid . $errid;
	}

	function getErrors()
	{
		return $this->errid;
	}

}
