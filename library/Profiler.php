<?php

class Profiler
{
	protected $marker;
	protected $currentPoint = 0;
	public $status = 'disable';
	
	function __construct()
	{
		$this->mark('start');
	}
	
	function enable()
	{
		$this->status = 'enable';
	}
	
	function disable()
	{
		$this->status = 'disable';
	}
	
	function mark($point)
	{
		$this->marker[$point]['time'] = $this->microtime_float();
		$this->marker[$point]['memory'] = $this->convert(memory_get_usage());
		
		if ($point != 'start')
		{
			$prevpoint = $this->currentPoint - 1;
			$markers = array_keys($this->marker);
			$this->calc($point, $this->marker[$point], $this->marker[$markers[$prevpoint]]);
		}
		$this->currentPoint++;
	}
	
	function microtime_float()
	{
		list($usec, $sec) = explode(' ', microtime());
		return ((float)$usec + (float)$sec);
	}
	
	function convert($size)
	{
		$newunit = round($size/1024, 2);
		return $newunit.' kb';
	}
	
	function calc($point, $current, $prev)
	{
		if ($point != 'runtime')
		{
			$this->marker[$point]['time_diff'] = number_format($current['time'] - $prev['time'], 2). ' sec';
			$this->marker[$point]['memory_diff'] = number_format($current['memory'] - $prev['memory'], 2).' kb';
		}
		else
		{
			$this->marker[$point] = number_format($current['time'] - $prev['time'], 2);
		}
	}
	
	function output()
	{
		$output = array();
		$this->mark('end');
		$this->calc('runtime', $this->marker['end'], $this->marker['start']);
		return $this->marker;
	}
	
}