<?php

class SSH2
{
	protected $conn;
	protected $username;
	protected $publicKeyFile;
	protected $privateKeyFile;

	public function __construct($host, $port = 22)
	{
		if (($this->conn = @ssh2_connect($host, $port)) === false)
		{
			$e = new ErrorManager("SSH2 unable to connect: $host");
			$e->handleError();
		}
	}
	
	function auth($username, $password=null, $publicKeyFile=null, $privateKeyFile=null)
	{
		$this->username = $username;
		$this->publicKeyFile = $publicKeyFile;
		$this->privateKeyFile = $privateKeyFile;
		
		if (!is_null($password))
		{
			if ((@ssh2_auth_password($this->conn, $username, $password)) === false)
			{
				$e = new ErrorManager("SSH2 login is invalid");
				$e->handleError();
			}
		}
		elseif (!is_null($publicKeyFile) && !is_null($privateKeyFile))
		{
			if ((@ssh2_auth_pubkey_file($this->conn, $username, $publicKeyFile, $privateKeyFile)) === false)
			{
				$e = new ErrorManager("SSH2 keys are invalid");
				$e->handleError();
			}
		}
		else
		{
			$e = new ErrorManager("SSH2 must specify password or keys");
			$e->handleError();
		}
	}
	
	function disconnect()
	{
		$this->ssh2_exec('exit;');
	}
	
	function __call($func, $args)
	{
		if ($func == 'recv' || $func == 'send')
		{
			$func = 'ssh2_scp_' . $func;
			$conn = $this->conn;
		}
		else {
			$func = 'ssh2_sftp_' . $func;
			$conn = @ssh2_ftp($this->conn);
		}
	
		if (function_exists($func))
		{
			array_unshift($args, $conn);
			return call_user_func_array($func, $args);
		}
		else 
		{
			$e = new ErrorManager("SSH2 $func is not a valid function");
			$e->handleError();
		}
	}
}