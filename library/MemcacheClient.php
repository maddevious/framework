<?php 

class MemcacheClient
{
	protected $memcache;
	protected $valid_types = array('SESS','OFF','STR','PROD','SOF');

	function __construct($memcacheServers)
	{	
		$this->memcache = new Memcache;
		foreach ($memcacheServers as $mc)
		{
			if (trim($mc) != "")
			{
				$this->memcache->addServer(trim($mc), 11211);
			}
		}
	}
	
	function get($type, $key)
	{
		$output = $output1 = array();
		if (!in_array($type, $this->valid_types))
		{
			$e = new ErrorManager("Invalid Memcache type - $type");
			$e->handleError();
		}
		if (!is_array($key))
		{
			$output = $this->memcache->get($type.'_'.$key);
		}
		else {
			foreach ($key as $k)
			{
				$keys[]=$type.'_'.$k;
			}
			$output = $this->memcache->get($keys);
		}
		if ($output == false)
		{
			return false;
		}
		if (is_array($key))
		{
			foreach ($output as $o => $oInfo)
			{
				$o = str_replace($type.'_', '', $o);
				$output1[$o] = $oInfo;
			}
			$output=$output1;
		}
		return $output;
	}
	
	function set($type, $key, $value, $expiration=0)
	{
		if (!in_array($type, $this->valid_types))
		{
			$e = new ErrorManager("Invalid Memcache type - $type");
			$e->handleError();
		}
		if ($key == '')
		{
			$e = new ErrorManager("Missing key parameter");
			$e->handleError();
		}
	
		return $this->memcache->set($type.'_'.$key, $value, 0, $expiration);
	}
	
	function delete($type, $key, $timeout=0)
	{
		if (!in_array($type, $this->valid_types))
		{
			$e = new ErrorManager("Invalid Memcache type - $type");
			$e->handleError();
		}
		if ($key == '')
		{
			$e = new ErrorManager("Missing key parameter");
			$e->handleError();
		}
		$this->memcache->delete($type.'_'.$key, $timeout);
		return true;
	}
}