<?php

class DB
{
    protected $connection;
    protected $connectionInfo;
    protected $sql;
    protected $statement_name;
    protected $result;
    protected $auditEnabled = false;
    protected $auditInfo = array();
    protected $trace;

    function __construct($connInfo)
    {
        $this->connectionInfo = $connInfo;
        $this->connect();
    }

    function connect()
    {
        $this->connectionInfo['user'] = !isset($this->connectionInfo['user']) ? 'famnetweb' : $this->connectionInfo['user'];
        $this->connectionInfo['password'] = !isset($this->connectionInfo['password']) ? '' : $this->connectionInfo['password'];
        $this->connectionInfo['dbname'] = !isset($this->connectionInfo['dbname']) ? 'famnet5' : $this->connectionInfo['dbname'];
        $this->connectionInfo['port'] = !isset($this->connectionInfo['port']) ? '5432' : $this->connectionInfo['port'];
        if ($this->connectionInfo['password'] != '')
        {
            $connectstring = "host={$this->connectionInfo['host']} port={$this->connectionInfo['port']} dbname={$this->connectionInfo['dbname']} user={$this->connectionInfo['user']} password={$this->connectionInfo['password']}";
        }
        else
        {
            $connectstring = "host={$this->connectionInfo['host']} port={$this->connectionInfo['port']} dbname={$this->connectionInfo['dbname']} user={$this->connectionInfo['user']}";
        }
        if (!$this->connection = pg_connect($connectstring))
        {
            $e = new ErrorManager('Unable to connect to database: ' . $connectstring);
            $e->handleError();
        }
    }

    function prepare($sql)
    {
        if (get_resource_type($this->connection) != 'pgsql link')
        {
            $e = new ErrorManager('Invalid Resource Type');
            $e->handleError();
        }
        $this->sql = html_entity_decode($sql);
        $this->statement_name = uniqid();
        if (!$this->result = @pg_prepare($this->connection, $this->statement_name, $this->sql))
        {
            $e = new ErrorManager('Unable to prepare statement: SQL: ' . $this->sql . ', Error: ' . pg_last_error($this->connection));
            $e->handleError();
        }
    }

    function execute()
    {
        if (get_resource_type($this->connection) != 'pgsql link')
        {
            $e = new ErrorManager('Invalid Resource Type');
            $e->handleError();
        }
        $parameters = func_get_args();
        $params = (isset($parameters[0]) && is_array($parameters[0])) ? $parameters[0] : $parameters;
        if (count($params) > 0)
        {
            array_walk($params, 'html_entity_decode');
        }

        if (!$this->result = @pg_execute($this->connection, $this->statement_name, $params))
        {
            $e = new ErrorManager('Unable to execute query: SQL: ' . $this->sql . ', Error: ' . pg_last_error($this->connection) . ' Parameters:' . print_r($params, true));
            $e->handleError();
        }
        $this->auditQuery();
    }

    function deallocate()
    {
        if (get_resource_type($this->connection) != 'pgsql link')
        {
            $e = new ErrorManager('Invalid Resource Type');
            $e->handleError();
        }
        $this->sql = 'DEALLOCATE ' . $this->statement_name;
        if (!$this->result = @pg_query($this->connection, $this->sql))
        {
            $e = new ErrorManager('Unable to dellocate prepare statement: SQL: ' . $this->sql . ', Error: ' . pg_last_error($this->connection));
            $e->handleError();
        }
    }

    function escapeString($parameter, $addQuotes = true)
    {
        if ($addQuotes)
        {
            $quote = "'" . @pg_escape_string($parameter) . "'";
        }
        else
        {
            $quote = @pg_escape_string($parameter);
        }
        return $quote;
    }

    function dropDB($database)
    {
        if (get_resource_type($this->connection) != 'pgsql link')
        {
            $e = new ErrorManager('Invalid Resource Type');
            $e->handleError();
        }
        $this->sql = 'DROP DATABASE IF EXISTS `' . pg_escape_string(trim($database), $this->connection) . '`';
        if (!$this->result = @pg_query($this->connection, $this->sql))
        {
            $e = new ErrorManager('Unable to drop database: SQL: ' . $this->sql . ', Error: ' . pg_last_error($this->connection));
            $e->handleError();
        }
    }

    function dropTable($table)
    {
        if (get_resource_type($this->connection) != 'pgsql link')
        {
            $e = new ErrorManager('Invalid Resource Type');
            $e->handleError();
        }
        $this->sql = 'DROP TABLE IF EXISTS `' . pg_escape_string(trim($table), $this->connection) . '`';
        if (!$this->result = @pg_query($this->connection, $this->sql))
        {
            $e = new ErrorManager('Unable to drop table: SQL: ' . $this->sql . ', Error: ' . pg_last_error($this->connection));
            $e->handleError();
        }
    }

    function startTransaction()
    {
        if (get_resource_type($this->connection) != 'pgsql link')
        {
            $e = new ErrorManager('Invalid Resource Type');
            $e->handleError();
        }
        $this->sql = "BEGIN;";
        if (!$this->result = @pg_query($this->connection, $this->sql))
        {
            $e = new ErrorManager('Unable to start transaction: Error: ' . pg_last_error($this->connection));
            $e->handleError();
        }
    }

    function commitTransaction()
    {
        if (get_resource_type($this->connection) != 'pgsql link')
        {
            $e = new ErrorManager('Invalid Resource Type');
            $e->handleError();
        }
        $this->sql = "COMMIT;";
        if (!$this->result = @pg_query($this->connection, $this->sql))
        {
            $e = new ErrorManager('Unable to commit transaction: Error: ' . pg_last_error($this->connection));
            $e->handleError();
        }
    }

    function rollback()
    {
        if (get_resource_type($this->connection) != 'pgsql link')
        {
            $e = new ErrorManager('Invalid Resource Type');
            $e->handleError();
        }
        $this->sql = "ROLLBACK;";
        if (!$this->result = @pg_query($this->connection, $this->sql))
        {
            $e = new ErrorManager('Unable to rollback transaction: Error: ' . pg_last_error($this->connection));
            $e->handleError();
        }
    }

    function fetchRow()
    {
        $rows = array();
        if (get_resource_type($this->connection) != 'pgsql link')
        {
            $e = new ErrorManager('Invalid Resource Type');
            $e->handleError();
        }
        if (!isset($this->result) || $this->result == false)
        {
            $e = new ErrorManager('No result object found: SQL: ' . $this->sql . ', Error: ' . pg_last_error($this->connection));
            $e->handleError();
        }
        $rows = @pg_fetch_assoc($this->result);
        return $rows;
    }

    function fetchAll()
    {
        if (get_resource_type($this->connection) != 'pgsql link')
        {
            $e = new ErrorManager('Invalid Resource Type');
            $e->handleError();
        }
        if (!isset($this->result) || $this->result == false)
        {
            $e = new ErrorManager('No result object found: SQL: ' . $this->sql . ', Error: ' . pg_last_error($this->connection));
            $e->handleError();
        }
        return pg_fetch_all($this->result);
    }

    function numRows()
    {
        if (!isset($this->result))
        {
            $e = new ErrorManager('No result object found: SQL: ' . $this->sql . ', Error: ' . pg_last_error($this->connection));
            $e->handleError();
        }
        if (($rows = @pg_num_rows($this->result)) === false)
        {
            $e = new ErrorManager('Unable to fetch number of rows: SQL: ' . $this->sql . ', Error: ' . pg_last_error($this->connection));
            $e->handleError();
        }
        return $rows;
    }

    function affectedRows()
    {
        if (!isset($this->result))
        {
            $e = new ErrorManager('No result object found: SQL: ' . $this->sql . ', Error: ' . pg_last_error($this->connection));
            $e->handleError();
        }
        if (($affected_rows = @pg_affected_rows($this->result)) === false)
        {
            $e = new ErrorManager('Unable to fetch affected rows: SQL: ' . $this->sql . ', Error: ' . pg_last_error($this->connection));
            $e->handleError();
        }
        return $affected_rows;
    }

    function nextVal($sequenceName)
    {
        if (get_resource_type($this->connection) != 'pgsql link')
        {
            $e = new ErrorManager('Invalid Resource Type');
            $e->handleError();
        }
        $this->prepare("select nextval ('$sequenceName') as nextval");
        $this->execute();
        $row = $this->fetchRow();

        return (int) $row['nextval'];
    }

    function close()
    {
        if ($this->connection)
        {
            @pg_close($this->connection);
            unset($this->connection);
        }
    }

    function enableAudit($table, $user, $sid, $fromip, $description, $areaid, $prior)
    {
        $this->_auditEnabled = true;
        $this->_auditInfo = array(
            'table'       => $table,
            'user'        => $user,
            'sid'         => $sid,
            'fromip'      => $fromip,
            'description' => $description,
            'areaid'      => $areaid,
            'prior'       => $prior
        );
    }

    function disableAudit()
    {
        $this->_auditEnabled = false;
        $this->_auditInfo = array();
    }

    function auditQuery()
    {
        if ($this->auditEnabled)
        {
            $db = new DB($this->sysconfig->xcdbserver);
            $db->prepare("
			INSERT INTO {$this->_auditInfo['table']} (userid,datetime,sessid,fromip,description,sqltext,priorval,areaid)
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
			");
            $db->execute($this->_auditInfo['user'], time(), $this->_auditInfo['sid'], $this->_auditInfo['fromip'], $this->_auditInfo['description'], $this->sql, $this->_auditInfo['prior'], $this->_auditInfo['areaid']);
            // Do not close connection as the Resource is shared by multiple objects
            unset($db);
        }
    }

    function createSQL($query_type, $table, $columns, $where = "")
    {
        $update_parts = array();
        $column_counter = 1;
        switch ($query_type)
        {
            case "update":
                foreach ($columns as $column_name => $column_value)
                {
                    $update_parts [] = "$column_name=$$column_counter";
                    $column_counter++;
                }

                $sql = "UPDATE $table
	        	        SET " . implode(",", $update_parts) . " " .
                        $where;

                break;
            case "insert":
                foreach ($columns as $column_name => $column_value)
                {
                    $update_parts [] = "$$column_counter";
                    $column_counter++;
                }

                $sql = "INSERT INTO $table ( " . implode(",", array_keys($columns)) . " )
	    		        VALUES ( " . implode(",", $update_parts) . ")";
                break;
        }
        return $sql;
    }
}