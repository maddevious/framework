<?php 

class SAML
{
	protected $publicKeyPath;
	protected $privateKeyPath;
	protected $SAMLAssertion;
	protected $recipientPath;
	protected $issuer;
	protected $spEntity;
	protected $assertionArray;
	protected $canonicalizationMethod;
	protected $signatureAlgorithm;
	protected $digestMethod;
	protected $canonicalizationMethodURL;
	protected $signatureAlgorithmURL;
	protected $digestMethodURL;
	protected $trimSignedInfo = true;
	protected $canonicalizeSignedInfo = true;
	protected $errors = array();
	protected $xpathFilename;
	protected $mode;
	protected $xml2array;

	function setConfig($assertion, $pubKeyPath, $recipientPath, $canonicalizationMethod = 'exclusive', $signatureAlgorithm = 'sha1', $digestMethod = 'sha1', $issuer='', $spEntity='', $xpathFilename='enveloped.xpath', $mode='--without-comments')
	{
		//for security reasons, the assertion is base64_encoded before it is POSTED to the eossso file.
		//It must be decoded now in order to be readable by the parser function.
		$this->SAMLAssertion = base64_decode($assertion);
		if($this->SAMLAssertion === false)
		{
			$this->errors[] = "SAML Assertion failure: Could not base64 decode";
			return false;
		}
		$this->publicKeyPath = $pubKeyPath;
		$this->recipientPath = $recipientPath;
		$this->issuer = $issuer;
		$this->spEntity = $spEntity;
		$this->xml2array = new XML2Array();
		$this->xml2array->createArray($this->SAMLAssertion);
		
		$this->xpathFilename = $xpathFilename;
		$this->mode = $mode;
		$this->canonicalizationMethod = $canonicalizationMethod;
		$this->signatureAlgorithm = $signatureAlgorithm;
		$this->digestMethod = $digestMethod;
			
		switch ($canonicalizationMethod)
		{
			default:
			case 'regular':
				$this->canonicalizationMethodURL = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
				break;
			case 'exclusive':
				$this->canonicalizationMethodURL = "http://www.w3.org/2001/10/xml-exc-c14n#";
				break;
		}

		switch ($signatureAlgorithm)
		{
			default:
			case 'sha1':
				$this->signatureAlgorithmURL = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
				break;
			case 'sha256':
				$this->signatureAlgorithmURL = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";
				break;
			case 'md5':
				$this->signatureAlgorithmURL = "http://www.w3.org/2001/04/xmldsig-more#rsa-md5";
				break;
		}


		switch ($digestMethod)
		{
			default:
			case 'sha1':
				$this->digestMethodURL = "http://www.w3.org/2000/09/xmldsig#sha1";
				break;
			case 'sha256':
				$this->digestMethodURL = "http://www.w3.org/2001/04/xmlenc#sha256";
				break;
			case 'md5':
				$this->digestMethodURL = "http://www.w3.org/2001/04/xmldsig-more#md5";
				break;
		}
	}


	function getErrors()
	{
		return $this->errors;
	}

	function getPublicKey()
	{
		$public_Key = openssl_pkey_get_public(file_get_contents($this->publicKeyPath));
		if (!$public_Key)
		{
			$this->errors[] = "Error opening public key file: ".$this->publicKeyPath;
			return false;
		}

		return $public_Key;
	}

	function getPrivateKey()
	{
		$private_Key = openssl_pkey_get_private(file_get_contents($this->privateKeyPath));
		if (!$private_Key)
		{
			$this->errors[] = "Error opening private key file: ".$this->privateKeyPath;
			return false;
		}
		return $private_Key;
	}

	function setPrivateKeyPath($path) 
	{
		$this->privateKeyPath = $path;
	}

 	function setTrimSignedinfo($value)
 	{
		$this->trimSignedinfo = $value;
	}
	
	function setCanonicalizeSignedinfo($value)
	{
		$this->canonicalizeSignedinfo = $value;
	}

	function checkResponseID($saml_Attributes, MemcacheClient $memcache)
	{
		$token = $saml_Attributes['ID'];
		$memcache_token = $memcache->get('SESS', $token);
		if ($memcache_token=='' || $memcache_token==false)
		{
			$memcache->set('SESS', $token, $token, 900);
		}
		else
		{
			$this->errors[] = "Token Already Used Failure: $token";
			return false;
		}
		return true;
	}


	function checkRecipient($saml_Attributes)
	{
		$recipient = '';
		$recipient = $saml_Attributes['Recipient'];
		if ($recipient=='') {
			$recipient = $saml_Attributes['Destination'];
		}
		if($recipient != $this->recipientPath)
		{
			$this->errors[] = "Invalid Recipient: expecting ".$this->recipientPath.", got ".$recipient;
			return false;
		}
		return true;
	}

	function checkIssuer($issuer)
	{
		if($issuer != $this->issuer)
		{
			$this->errors[] = "Invalid Issuer: expecting ".$this->issuer.", got ".$issuer;
			return false;
		}
		return true;
	}

	function checkspEntity($sp_entity)
	{
		if($sp_entity != $this->spEntity)
		{
			$this->errors[] = "Invalid SP Entity: expecting ".$this->spEntity.", got ".$sp_entity;
			return false;
		}
		return true;
	}

	function checkCanonicalizationMethod($method)
	{
		if ($method != $this->canonicalizationMethodURL)
		{
			$this->errors[] = "Invalid Canonicalization Method: expecting ".$this->canonicalizationMethodURL.", got ".$method;
			return false;
		}
		return true;
	}

	function checkSignatureAlgorithm($algorithm)
	{
		if ($algorithm != $this->signatureAlgorithmURL)
		{
			$this->errors[] = "Invalid Signature Algorithm: expecting ".$this->signatureAlgorithmURL.", got ".$algorithm;
			return false;
		}
		return true;
	}

	function checkDigestMethod($digest_Method)
	{
		if ($digest_Method != $this->digestMethodURL)
		{
			$this->errors[] = "Invalid Digest Method: expecting ".$this->digestMethodURL.", got ".$digest_Method;
			return false;
		}
		return true;
	}

	function checkSignatureDigest($digest)
	{
		$XMLSecurity = new XMLSecurity($this->SAMLAssertion, $this->xpathFilename, $this->mode);
		$calcdigest = $XMLSecurity->createSignatureDigest($this->digestMethod);
		if ($digest != $calcdigest)
		{
			$this->errors[] = "Invalid Signature Digest: expecting $digest, got $calcdigest";
			return false;
		}
		return true;
	}

	function checkDigitalSignature($signature)
	{
		$bsig = base64_decode(str_replace(' ', '', trim($signature)));
		if($bsig === FALSE)
		{
			$this->errors[] = "Invalid Signature: it was not binary";
			return false;
		}
		$XMLSecurity = new XMLSecurity($this->SAMLAssertion, $this->xpathFilename, $this->mode);
		$sign3 = $XMLSecurity->createSignInfo($this->canonicalizeSignedInfo, $this->trimSignedInfo);

		if (!$XMLSecurity->verifySignature($sign3, $bsig, $XMLSecurity->getPublicKey($this->publicKeyPath), $this->signatureAlgorithm))
		{
			$this->errors[] = "Signature does not verify against key: ".openssl_error_string()." -- signinfo:".$sign3;
			return false;
		}
		return true;
	}

	function checkStatusCode($status)
	{
		if (!stristr($status, 'Success'))
		{
			$this->errors[] = "Status code not successful: ".$status;
			return false;
		}
		return true;
	}

	function checkExpirationTime($not_Before, $not_On_Or_After)
	{
		$XMLSecurity = new XMLSecurity($this->SAMLAssertion, $this->xpathFilename, $this->mode);
		if (!$XMLSecurity->verifyExpirationTime())
		{
			$this->errors[] = "Expiration Time Fail: ".$not_Before." (".$this->timestampParse($not_Before).") ".$not_On_Or_After." (".$this->timestampParse($not_On_Or_After)."), current time ".time();
			return false;
		}
		return true;
	}

	function decryptAssertion($encrypted_assertion, $encrypted_key, $padding = OPENSSL_PKCS1_PADDING)
	{
		if (openssl_private_decrypt($encrypted_key, $decrypted_symmetric_key, $this->getPrivateKey(), $padding))
		{
			openssl_free_key($this->getPrivateKey());
		}
		else
		{
			$this->errors[] = "openssl_private_decrypt() failed: ".openssl_error_string();
		}
		
		$decrypted_SAML = rtrim(@mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $decrypted_symmetric_key, $encrypted_assertion, MCRYPT_MODE_CBC));
		preg_match('/<saml.*Assertion>/s',$decrypted_SAML, $matches);
		$decrypted_SAML = $matches[0];
		return $decrypted_SAML;
	}

	function getValue($key)
	{
		return $this->xml2array->getValue($key);
	}
	
	function getAttribute($key, $name = '')
    {
    	return $this->xml2array->getAttribute($key, $name);
    }
}