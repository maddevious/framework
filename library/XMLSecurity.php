<?php 

class XMLSecurity
{
	protected $xml;
	protected $xpathFilename;
	protected $mode;
	
	function __construct($xml, $xpathFilename = '', $mode = '--with-comments')
	{
		$this->xml = $xml;
		$this->xpathFilename = $xpathFilename;
		$this->mode = $mode;
	}
	
	function canonicalizeXML($xml = '')
	{
		$xml = isset($xml) ? $xml : $this->xml;
		$data="xml=".base64_encode($xml)."&xpath=".$this->xpathFilename."&mode=".$this->mode;
		$output = HTTP::request('http://172.16.16.21/xmlrpc_api.php', $data, 'POST');
		$reordered = base64_decode($output['body']);
	
		if (trim($reordered) == '')
		{
			$e = new ErrorManager("Failed to canonicalize XML file: ".$this->xml);
			$e->handleError(false);
			return false;
		}
		return $reordered;
	}
	
	function createSignatureDigest($digestMethod, $xml_tag = '')
	{
		if ($xml_tag != '')
		{
			$xml = $this->canonicalizeXML($this->xml, $xml_tag);
		}
		else
		{
			$xml = $this->canonicalizeXML($this->xml, $this->xpathFilename, $this->mode);
		}
	
		if ($xml_tag != '')
		{
			preg_match_all("|<$xml_tag.*</$xml_tag>|", $xml, $matches);
			$xml = $matches[0][0];
		}
	
		$calcdigest = Security::hash($xml, $digestMethod);
		return $calcdigest;
	}
	
	function createSignInfo($canonicalize = true, $trim = true)
	{
		$count = 0;
		$matches = '';
		$signature_Regex = '|<([a-z]+):Signature.*?</\\1:Signature>|ms';
		$signature_Regex2 = '|<Signature.*?</Signature>|ms';
		$c14n_Xml = preg_replace('|<\\?xml.*\\?>\n?|','',$this->xml);
		$detagged = preg_replace('|<([a-zA-Z1-9:]+)([^>]*?)?/>|ms','<\\1\\2></\\1>', $c14n_Xml);
		$count = preg_match_all($signature_Regex, $detagged, $matches);
		$envelope = isset($matches[0][0]) ? $matches[0][0] : '';
		if (trim($envelope) == '')
		{
			$count = preg_match_all($signature_Regex2, $detagged, $matches);
			$envelope = $matches[0][0];
		}
		$signedinfomatches = '';
		$signedinfostatus = preg_match_all('|<([a-z]+):SignedInfo[^a-z].*</\\1:SignedInfo>|ms', $envelope, $signedinfomatches);
		$signedinfo =  isset($signedinfomatches[0][0]) ? $signedinfomatches[0][0] : '';
		if (trim($signedinfo) == '')
		{
			$signedinfostatus = preg_match_all('|<SignedInfo[^a-z].*</SignedInfo>|ms', $envelope, $signedinfomatches);
			$signedinfo = $signedinfomatches[0][0];
		}
		$sign3 = preg_replace("/<ds:SignedInfo>/",'<ds:SignedInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#">', $signedinfo);
		$sign3 = preg_replace("/<SignedInfo>/",'<SignedInfo xmlns="http://www.w3.org/2000/09/xmldsig#">', $sign3);
		if ($canonicalize)
		{
			$sign3 = $this->canonicalizeXML($sign3);
		}
	
		$sign_array = Arrays::stringToArray($sign3, "\n");
		foreach ($sign_array as $i)
		{
			if ($trim)
			{
				$sign_array_output[] = trim($i);
			}
			else {
				$sign_array_output[] = rtrim($i);
			}
		}
		$sign3=implode("\n", $sign_array_output);
		//echo base64_encode($sign3); exit;
		return $sign3;
	}
	
	function getPublicKey($publicKeyPath)
	{
		$public_Key = openssl_pkey_get_public(file_get_contents($publicKeyPath));
		if (!$public_Key)
		{
			$e = new ErrorManager("Error opening public key file: ".$publicKeyPath);
			$e->handleError(false);
			return false;
		}
	
		return $public_Key;
	}
	
	function getPrivateKey($privateKeyPath)
	{
		$private_Key = openssl_pkey_get_private(file_get_contents($privateKeyPath));
		if (!$private_Key)
		{
			$e = new ErrorManager("Error opening private key file: ".$privateKeyPath);
			$e->handleError(false);
			return false;
		}
		return $private_Key;
	}
	
	function createDigitalSignature($privateKey, $algo, $digest)
	{
		$signinfo = $this->createSignInfo();
		$signinfo = str_replace('|DIGESTVALUE|', $digest, $signinfo);
		$algo = strtolower($algo);
		
		if ($algo == 'md5')
		{
			$algoSSL = OPENSSL_ALGO_MD5;
		}
		elseif ($algo == 'sha1')
		{
			$algoSSL = OPENSSL_ALGO_SHA1; 
		}
		elseif ($algo == 'sha256')
		{
			$algoSSL = OPENSSL_ALGO_SHA256;
		}
		elseif ($algo == 'sha512')
		{
			$algoSSL = OPENSSL_ALGO_SHA512;
		}
		else
		{
			$e = new ErrorManager("Unsupported algorithm");
			$e->handleError();
		}
	
		if (openssl_sign($signinfo, $signature, $privateKey, $algoSSL) != 1)
		{
			$e = new ErrorManager("Unable to create signature: ".openssl_error_string()." -- signinfo: ".$signinfo);
			$e->handleError(false);
			return false;
		}
		return base64_encode($signature);
	}
	
	function createTimestamp($time = '')
	{
		$time = ($time=='') ? time() : $time;
		$start = gmdate('Y-m-d\TH:i:s\Z', $time);
		$end = gmdate('Y-m-d\TH:i:s\Z', $time+300);
		return array($start, $end);
	}
	
	function timestampParse($string)
	{
		$string = preg_replace("/T/"," ",$string);
		$string = preg_replace("/Z$/"," GMT",$string);
		return strtotime($string);
	}
	
	function verifySignature($signInfo, $signature, $publicKey, $algo)
	{
		if (openssl_verify($signInfo, $signature, $publicKey, $algo) != 1)
		{
			$e = new ErrorManager("Signature does not verify against key: ".openssl_error_string()." -- signinfo:".$signInfo);
			$e->handleError(false);
			return false;
		}
		return true;
	}
	
	function verifyExpirationTime($not_Before, $not_On_Or_After)
	{
		$timestamp = $this->createTimestamp();
		$time = $timestamp[0];
		$time = $this->timestampParse($time);
	
		if (($this->timestampParse($not_Before)-300 <= $time) != 1)
		{
			$e = new ErrorManager("Expiration Time: Not valid yet ".$not_Before."(".$this->timestampParse($not_Before)."), current time ".$time);
			$e->handleError(false);
			return false;
		}
	
		if (($this->timestampParse($not_On_Or_After)+300 >$time) != 1)
		{
			$e = new ErrorManager("Expiration Time: Assertion expired ".$not_On_Or_After."(".$this->timestampParse($not_On_Or_After)."), current time ".$time);
			$e->handleError(false);
			return false;
		}
	
		if(($this->timestampParse($not_Before)-300 > $time) || ($this->timestampParse($not_On_Or_After)+300 <= $time))
		{
			$e = new ErrorManager("Expiration Time Frame: ".$not_Before." (".$this->timestampParse($not_Before).") ".$not_On_Or_After." (".$this->timestampParse($not_On_Or_After)."), current time ".$time);
			$e->handleError(false);
			return false;
		}
		return true;
	}
}