<?php

class Form
{

	protected $input;
	protected $html;
	protected $action;

	function __construct($action = null, $id = '', $name = '', $method = 'post')
	{
		$this->input = new Input();
		$this->action = $action;
		if (!is_null($action))
		{
			$this->html = '<form method="' . $method . '" action="' . $action . '" id="' . $id . '" name="' . $name . '"><input type="hidden" name="formsubmit" value="t" />' . "\n";
		}
	}

	function create($action, $id, $name, $method)
	{
		$this->action = $action;
		$this->html = '<form method="' . $method . '" action="' . $action . '" id="' . $id . '" name="' . $name . '"><input type="hidden" name="formsubmit" value="t" />' . "\n";
	}
	
    function resetForm()
    {
        $this->input = $this->html = $this->action = '';
    }

	function inputElement($name, $value = null, $type = 'text', $parameters = array())
	{
		$value = is_null($value) ? $this->input->request($name) : $value;
		$parameterString = 'type="' . $type . '" value="' . htmlentities($value) . '" name="' . $name . '"';
		foreach ($parameters as $pkey => $pvalue)
		{
			$parameterString .= ' ' . $pkey . '="' . $pvalue . '"';
		}
		$this->html .= '<input ' . $parameterString . ' />' . "\n";
	}

	function dropdownElement($name, $value = null, $list = array(), $parameters = array())
	{
		$value = is_null($value) ? $this->input->request($name) : $value;
		$parameterString = 'name="' . $name . '"';
		foreach ($parameters as $pkey => $pvalue)
		{
			$parameterString .= ' ' . $pkey . '="' . $pvalue . '"';
		}
		$this->html .= '<select ' . $parameterString . '>' . "\n";
		foreach ($list as $k => $option)
		{
			$selected[$k] = ($k == $value || (isset($option['selected']) && $option['selected'] == true)) ? 'selected="selected"' : '';
			$this->html .= '<option value="' . $k . '" ' . $selected[$k] . '>' . $option['value'] . '</option>' . "\n";
		}
		$this->html .= '</select>' . "\n";
	}

	function textareaElement($name, $value = null, $parameters = array())
	{
		$parameterString = 'name="' . $name . '"';
		foreach ($parameters as $pkey => $pvalue)
		{
			$parameterString .= ' ' . $pkey . '="' . $pvalue . '"';
		}
		$value = is_null($value) ? $this->input->request($name) : $value;
		$this->html .= '<textarea ' . $parameterString . '>' . htmlentities($value) . '</textarea>' . "\n";
	}

	function radioElement($name, $value, $parameters = array(), $checked = false)
	{
		$checked = $this->input->request($name) == $value || (isset($checked) && $checked == true) ? 'checked="checked"' : '';
		$parameterString = ' type="radio" name="' . $name . '" value="' . htmlentities($value) . '"';
		foreach ($parameters as $pkey => $pvalue)
		{
			$parameterString .= ' ' . $pkey . '="' . $pvalue . '"';
		}
		$parameterString .= $checked;
		$this->html .= '<input ' . $parameterString . ' />' . "\n";
	}

	function checkboxElement($name, $value, $parameters = array())
	{
		if (strstr($name, '[') && strstr($name, ']'))
		{
			$q = preg_replace('/\[.*\]/', '', $name);
			preg_match('/\[.*\]/', $name, $match);
			$var = $match[0];
			$data = $this->input->request($q);
			$result = '';
			eval('$result = isset($data' . $var . ') ? $data' . $var . ' : "";');
			$checked = $result == $value || (isset($checked) && $checked == true) ? 'checked="checked"' : '';
		}
		else
		{
			$checked = $this->input->request($name) == $value || (isset($checked) && $checked == true) ? 'checked="checked"' : '';
		}
		$parameterString = 'type="checkbox" name="' . $name . '" value="' . htmlentities($value) . '"';
		foreach ($parameters as $pkey => $pvalue)
		{
			$parameterString .= ' ' . $pkey . '="' . $pvalue . '"';
		}
		$parameterString .= $checked;
		$this->html .= '<input ' . $parameterString . ' />' . "\n";
	}

	function buttonElement($type, $name, $value, $parameters = array())
	{
		if ($type != 'button' && $type != 'submit' && $type != 'reset')
		{
			$e = new ErrorManager('Invalid button element type: ' . $type);
			$e->handleError();
		}
		$parameterString = 'type="' . $type . '" name="' . $name . '" value="' . htmlentities($value) . '"';
		foreach ($parameters as $pkey => $pvalue)
		{
			$parameterString .= ' ' . $pkey . '="' . $pvalue . '"';
		}
		$this->html .= '<input ' . $parameterString . ' />';
	}

	function labelElement($for, $label, $parameters = array())
	{
		$parameterString = 'for="' . $for . '"';
		foreach ($parameters as $pkey => $pvalue)
		{
			$parameterString .= ' ' . $pkey . '="' . $pvalue . '"';
		}
		$this->html .= '<label ' . $parameterString . '>' . $label . '</label>' . "\n";
	}

	function fieldsetElement()
	{
		
	}

	function anchorElement($href, $label, $parameters = array())
	{
		$parameterString = '';
		if (!empty($parameters))
		{
			foreach ($parameters as $k => $v)
			{
				$parameterString = ' ' . $k . ' = "' . $v . '"';
			}
		}
		$this->html .= '<a href="' . $href . '" ' . $parameterString . '>' . $label . '</a>' . "\n";
	}

	function imageElement($image, $parameters = array())
	{
		$parameterString = '';
		if (!empty($parameters))
		{
			foreach ($parameters as $k => $v)
			{
				$parameterString = ' ' . $k . ' = "' . $v . '"';
			}
		}
		$this->html .= '<img src="' . $image . '" ' . $parameterString . ' />' . "\n";
	}

	function imageButtonElement($image, $width, $height, $alt = '', $class = '', $id = '')
	{
		$this->html .= '<a href="javascript:;" class="' . $class . '" id="' . $id . '"><img src="' . $image . '" width="' . $width . '" height="' . $height . '" alt="' . $alt . '" /></a>' . "\n";
	}

	function html($html)
	{
		$this->html .= $html . "\n";
	}

	function getForm()
	{
		if (!is_null($this->action))
		{
			$this->html .= '</form>' . "\n";
		}
        $html = $this->html;
        $this->resetForm();
		return $html;
	}

}
