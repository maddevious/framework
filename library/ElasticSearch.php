<?php 

class ElasticSearch
{
	protected $filter = array();
	protected $host = 'localhost';
	
	function __construct($host = null)
	{
		if (!is_null($host))
		{
			$this->host = $host;
		}
	}
	
	function setHost($host)
	{
		$this->host = $host;
	}

	function get($collection, $document, $key, $from = 0, $size = 20)
	{
		if ($key == '')
		{
			$data['query']['match_all'] = array();
			$output = $this->execute('XGET', $collection, $document, '_search', $data, $from, $size);
		}
		else
		{
			$data['ids'] = $key;
			$output = $this->execute('XGET', $collection, $document, '_mget', $data, $from, $size);
		}
		return $output;
	}
	
	function insert($collection, $document, $key = '', $data)
	{
		$output = $this->execute('XPOST', $collection, $document, $key, $data);
		return $output;	
	}
	
	function update($collection, $document, $key, $data)
	{
		$output = $this->execute('XPOST', $collection, $document, $key.'/_update', $data);
		return $output;
	}
	
	function delete($collection, $document, $key)
	{
		$output = $this->execute('XDELETE', $collection, $document, $key);
		return $output;
	}
	
	function search($collection, $document, $query = null, $from = 0, $size = 20)
	{
		$query['aggs'] = isset($query['aggs']) ? $query['aggs'] : array();
		$query['query'] = isset($query['query']) ? $query['query'] : array();
		$query['filter'] = isset($query['filter']) ? $query['filter'] : array();
		$query['script_fields'] = isset($query['script_fields']) ? $query['script_fields'] : array();
		$query['sort'] = isset($query['sort']) ? $query['sort'] : array();		
		
		$input['fields'][] = '_source';
		if (!empty($query['query'])) $input['query']['filtered']['query'] = $query['query'];
		if (!empty($query['filter'])) $input['query']['filtered']['filter'] = $query['filter'];
		if (!empty($query['script_fields'])) $input['script_fields'] = $query['script_fields'];
		if (!empty($query['sort'])) $input['sort'] = $query['sort'];
		if (!empty($query['aggs'])) $input['aggs'] = $query['aggs'];
	
		$output = $this->execute('XGET', $collection, $document, '_search', $input, $from, $size);
		return $output;
	}

	function bulkInsert($collection, $document, $data, $keyed = false, $chunkSize = 1000)
	{
		if (!is_array($data) || empty($data))
		{
			$e = new ErrorManager("Data is not an array or empty");
			$e->handleError();
		}
		$chunks = array_chunk($data, $chunkSize);
		foreach ($chunks as $i => $chunk)
		{
			$dataFile = tempnam("/tmp", "esbulk");
			$fh = fopen($dataFile,'w');
			foreach ($chunk as $i => $d)
			{
				$newData = array();
				$newData['index'] = array('_index' => $collection, '_type' => $document);
				if ($keyed)
				{
					 $newData['index']['_id'] = $i;
				}
				$dataString = '';
				$dataString .= Arrays::json_encode($newData)."\n";
				$dataString .= Arrays::json_encode($d)."\n";
				fwrite($fh, $dataString, strlen($dataString));
			}
			fclose($fh);
			$output = $this->execute('XPOST', $collection, $document, '_bulk', $dataFile);
			unlink($dataFile);
		}
		return $output;
	}
	
	function createCollection($collection)
	{
		$output = $this->execute('XPUT', $collection, '', '');
		return $output;
	}
	
	function deleteCollection($collection)
	{
		$output = $this->execute('XDELETE', $collection, '', '');
		return $output;
	}
	
	function createDocument($collection, $document)
	{
		$output = $this->execute('XPUT', $collection, $document, '');
		return $output;
	}
	
	function deleteDocument($collection, $document)
	{
		$output = $this->execute('XDELETE', $collection, $document, '');
		return $output;
	}
	
	function createMapping($collection, $document, $mapping)
	{
		$output = $this->execute('XPUT', $collection, $document, '_mapping', $mapping);
		return $output;
	}
	
	function execute($method, $collection, $document, $endpoint, $data = null, $from = 0, $size = 20)
	{	
		if ($endpoint == '_bulk')
		{
			$command = "curl -N -s -{$method} 'http://{$this->host}:9200/{$endpoint}' --data-binary @{$data}";
		}
		else
		{
			$data['from'] = $from;
			$data['size'] = intval($size);
			$json = empty($data) ? '' : Arrays::json_encode($data);
			$collection = $collection != '' ? $collection.'/' : '';
			$document = $document != '' ? $document.'/' : '';
			$command = "curl -N -s -{$method} 'http://{$this->host}:9200/{$collection}{$document}{$endpoint}' -d '".$json."'";
		}
		//echo "$command<br><br>";
		exec($command, $return);
		$return = implode("", $return);
		return Arrays::json_decode($return, true);
	}
}