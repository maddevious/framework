Description
--------------------------------
A custom built framework which allows a developer to easily build a complete web site or web-based tools in a modular and standardize MVC format.  The framework is lightweight and custom modules can be easily integrated.  Framework has built-in security features such as cross site scripting prevention, SQL injection prevention, and various data cleaning and validation functions allowing rapid development. 

Requirements
--------------------------------
* Linux (Ubuntu)
* PHP5
* Apache2

License
--------------------------------

Ownership of this Software belongs to Jimmy Morgan and by installing/using the Software you agree:
1) Not to alter, copy, modify or re-transmit the Software
2) Not to lease, license, rent, or sell the Software or the right to use and access the Software
3) Not to remove, obscure, or alter any text or proprietary notices contained in Software
4) Not to copy or imitate part or all of the design, layout, or look-and-feel of the Software
5) Software is non-transferable
6) Software may only be used to evaluate coding, and may not be used for any other purposes

Jimmy Morgan
957 Cherrywood Dr
Baldwin, NY 11510
(908) 763-6991