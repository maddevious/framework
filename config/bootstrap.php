<?php
$hostname = exec('hostname -f');
if (stristr($hostname, 'admin') || stristr($hostname, 'beta') || stristr($hostname, 'dev'))
{
	error_reporting(E_ALL);
	define('DEBUG', true);
	ini_set('display_errors', '1');
}
else {
	define('DEBUG', false);
	ini_set('display_errors', '0');
}

//date_default_timezone_set('America/New_York');

//spl_autoload_register('my_autoload');
include_once SYSPATH."/core/Controller.php";
include_once SYSPATH."/core/Input.php";
include_once SYSPATH."/core/Loader.php";
include_once SYSPATH."/core/Model.php";
include_once SYSPATH."/core/Output.php";
include_once SYSPATH."/helper/Security.php";

/*
function my_autoload($class_name)
{
	@include_once SYSPATH."/core/$class_name.php";
	@include_once SYSPATH."/helper/$class_name.php";
	@include_once SYSPATH."/library/$class_name.php";
	@include_once APPPATH."/library/$class_name.php";
}
 */

function dumpArray($vars)
{
	echo "<pre>".print_r($vars, true)."</pre>";
}

function callingFunction()
{
	$callers = debug_backtrace();
	return isset($callers[2]['function']) ? $callers[2]['function'] : '';
}