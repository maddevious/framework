<?php

class HTTP
{
	static function request($url, $data='', $method='GET', $data_type='HTTP', $header='')
	{
		$method = strtoupper($method);
		$methods = array('GET', 'POST');
		$data_types = array('HTTP', 'XML', 'JSON');

		if (!filter_var($url, FILTER_VALIDATE_URL))
		{
			$e = new ErrorManager('Invalid URL: '.$url);
			$e->handleError();
		}
		if (!in_array($method, $methods))
		{
			$e = new ErrorManager("Invalid method: {$method} in request");
			$e->handleError();
		}
		if (!in_array($data_type, $data_types))
		{
			$e = new ErrorManager("Invalid data type: {$data_type} in request");
			$e->handleError();
		}
		$dataString = '';
		if ($data != '' && $method == 'GET')
		{
			$dataArray = Arrays::stringToArray($data, '&');
			foreach ($dataArray as $d)
			{
				list($k, $v) = explode("=", $d);
				$dataString.= urlencode($k)."=".urlencode($v)."&";
			}
		}
		else
		{
			$dataString = $data;
		}
		$dataString = trim($dataString, '&');
		$request = 'curl -i -s -k -L';
		if ($method == 'POST')
		{
			if ($data_type == 'XML')
			{
				$request.= ' --header "Content-Type: application/xml"';
			}
			elseif ($data_type =='JSON')
			{
				$request.= ' --header "Content-Type: application/json"';
			}
			if ($header != '')
			{
				$request.= ' --header "'.$header.'"';
			}
			if ($data != '')
			{
				$request.= ' --data "'.$dataString.'" ';
			}
		}
		else
		{
			if ($data!='')
			{
				$url.="?$data";
			}
		}
		$request.= ' "'.$url.'"';
		//echo $request; exit;
		exec($request, $output);
		$xml_output = implode("\n",$output);
		$parts = explode("HTTP/", $xml_output);
		foreach ($parts as $i => $d)
		{
			if (trim($d) == '')
			{
				unset($parts[$i]);
			}
		}
		$parts = (count($parts) > 1 ? 'HTTP/' : '').array_pop($parts);
		@list($getheader, $body) = explode("\n\n", $parts, 2);
		$output = array('header' => $getheader, 'body' => $body);
		return $output;
	}

	static function redirect($url)
	{
		$input = new Input();
		$url = substr($url, 0, 1) == '/' ? ltrim($url, '/') : $url;
		$url = (!stristr($url, 'http')) ? "https://".$_SERVER['HTTP_HOST']."/$url" : $url;

		if($input->request('request_type') == 'ajax')
		{
			echo Arrays::json_encode(array('redirect' => $url));
			exit;
		}

		if (!filter_var($url, FILTER_VALIDATE_URL))
		{
			$e = new ErrorManager('Invalid URL: '.$url);
			$e->handleError();
		}
		if (php_sapi_name() != 'cli')
		{
			header("Location: $url");
		}
		exit;
	}

	static function soapRequest($xmlRequest, $soapAction, $endPoint, $header = null, $cert = null, $cacert = null, $user_pass = null, $timeout = 60)
	{
		$xml_tmp = tempnam("/tmp", "soap");
		file_put_contents($xml_tmp, $xmlRequest);
		$request = 'curl -s -k --connect-timeout '.$timeout.' -H SOAPAction: \"'.$soapAction.'\"" -H "Content-Type: text/xml; charset=UTF-8" ';
		if (!is_null($header))
		{
			$request .= $header.' ';
		}
		if (!is_null($cert))
		{
			$request .= ' -E '.$cert;
		}
		if (!is_null($cert))
		{
			$request .= ' --cacert '.$cacert;
		}
		if (!is_null($user_pass))
		{
			if (!strstr($user_pass, ':'))
			{
				$e = new ErrorManager('Invalid User/Password (format should be user:password');
				$e->handleError();
			}
			$request .= ' --user '.$user_pass;
		}
		$request .= ' --data-binary @'.$xml_tmp.' '.$endPoint;
		exec($request, $output);
		unlink($xml_tmp);
		$xml_output = implode("\n",$output);
		$parts = explode("HTTP/", $xml_output);
		foreach ($parts as $i => $d)
		{
			if (trim($d) == '')
			{
				unset($parts[$i]);
			}
		}
		$parts = (count($parts) > 1 ? 'HTTP/' : '').array_pop($parts);
		list($getheader, $body) = explode("\n\n", $parts, 2);
		$output = array('header' => $getheader, 'body' => $body);
		return $output;
	}
	
	static function APIClient($passphrase, $method, $params = null)
	{
		$paramsString = empty($params) ? '' : http_build_query($params);
		$json = HTTP::request("https://172.16.1.179/e/api.php", "passphrase=$passphrase&method=$method&$paramsString", 'POST');
		//dumpArray($json); exit;
		$output = Arrays::json_decode($json['body']);
		$output['status'] = !isset($output['status']) ? 'fail' : $output['status'];
		$output['error'] = !isset($output['error']) ? array('Z' => 'General API Failure') : $output['error'];
		$output['results'] = !isset($output['results']) ? array() : $output['results'];
		return $output;
	}
}