<?php

class Page
{
	static function currentPage()
	{
		$file = basename($_SERVER['SCRIPT_NAME']);
		$page = str_replace('.php', '', $file);
		return $page;
	}
	
	static function isAPI()
	{
		if (strstr($_SERVER['PHP_SELF'], '/api'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	static function paginate($results, $page, $per_page)
	{
		$output = array('total_pages' => 0, 'results' => array());
			
		$matches = count($results);
		$output['total_pages'] = ceil($matches / $per_page);
		$pg_start = ($per_page * $page) - ($per_page - 1);
		$ar_start = $pg_start - 1;
		if ($matches > 0)
		{
			$output['results'] = array_slice($results, $ar_start, $per_page, true);
		}
		return $output;
	}
}