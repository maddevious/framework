<?php

class Cookie
{
	static function set($name, $value, $expire=0, $path = '/', $domain = '', $secure = true, $httponly = true)
	{
		setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);
	}
	
	static function delete($name, $path = '/', $domain = '', $secure = true, $httponly = true)
	{
		setcookie($name, '', time() - 3600, $path, $domain, $secure, $httponly);
	}
	
	static function get($name)
	{
		$input = new Input();
		return $input->cookie($name);
	}
}