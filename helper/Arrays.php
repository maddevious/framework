<?php

class Arrays
{
	static function flattenArray($array, $result = array())
	{
		foreach ($array as $k => $v)
		{
			if (is_array($array[$k]))
			{
				$result = self::flattenArray($array[$k], $result);
			}
			else
			{
				$result[$k] = $v;
			}
		}
		return $result;
	}
	
	static function objectToArray($obj)
	{
		$arrObj = is_object($obj) ? get_object_vars($obj) : $obj;
		$arr = array();
		if (count($arrObj) > 0)
		{
	        foreach ($arrObj as $key => $val)
	        {
				$val = (is_array($val) || is_object($val)) ? self::objectToArray($val) : $val;
				$arr[$key] = $val;
	        }
		}
        return $arr;
	}
	
	static function arrayShuffle($array)
	{
		$keys = array_keys($array);
		shuffle($keys);
		foreach($keys as $key)
		{
			$new[$key] = $array[$key];
		}
		$array = $new;
		return $array;
	}
	
	static function arrayUnique($array)
	{
		return array_keys(array_flip($array));
	}
	
	static function natrsort(&$array)
	{
		natsort($array);
		$array = array_reverse($array, true);
	}
	
	static function extractVars($file, $vars)
	{
		$output = array();
	
		//clean file path here
		include $file;
		if (is_array($vars))
		{
			foreach ($vars as $v)
			{
				$output[$v] = $$v;
			}
		}
		else
		{
			$output = $$vars;
		}
		return $output;
	}
	
	static function extractNonArrays($obj)
	{
		$output = array();
		if (!empty($obj))
		{
			foreach ($obj as $k => $v)
			{
				if (!is_array($v) && !is_object($v) && $v != '')
				{
					$output[$k] = $v;
				}
			}
		}
		return $output;
	}
	
	static function stringToArray($string, $delimiter = ',')
	{
		return $string != '' ? explode($delimiter, $string) : array();
	}
	
	static function aasort($array, $key)
	{
	    $sorter=array();
	    $ret=array();

	    foreach ($array as $ii => $va)
	    {
	        $sorter[$ii]=$va[$key];
	    }
	    asort($sorter);
	    foreach ($sorter as $ii => $va)
	    {
	        $ret[$ii]=$array[$ii];
	    }
	    return $ret;
	}
	
	static function urldecodeArray($array)
	{
		foreach ($array as $k => $v)
		{
			$array[$k] = is_array($v) ? self::urldecodeArray($v) : urldecode($v);
		}
		return $array;
	}
	
	static function json_encode($data, $options = 0)
	{
		return json_encode($data, $options);
	}
	
	static function json_decode($json, $assoc = true)
	{
		$json = str_replace(array("\n","\r"),"", trim($json));
		return json_decode($json, $assoc);
	}
}