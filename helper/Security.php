<?php

class Security
{

	static function xssClean($var, $type = 'string')
	{
		if (is_array($var))
		{
			foreach ($var as $k => $v)
			{
				$var[$k] = self::xssClean($v, $type);
			}
		}
		else
		{
			$var = filter_var($var, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
			if ($type == 'json')
			{
				$var = htmlspecialchars_decode($var);
			}
			if (strpos($var, "\r") !== false)
			{
				$var = str_replace(array("\r\n", "\r"), "\n", $var);
			}
		}

		return $var;
	}

	static function cleanDate($dbDate = '', $day = '', $month = '', $year = '')
	{
		if ($dbDate != '')
		{
			@list($year, $month, $day) = explode("-", $dbDate);
		}
		else
		{
			$dbDate = "$year-" . str_pad($month, 2, '0', STR_PAD_LEFT) . "-" . str_pad($day, 2, '0', STR_PAD_LEFT);
		}

		if ($dbDate != '--' && preg_match('/^[12][0-9]{3}-(0[1-9]|1[0-2]|[1-9])-(0[1-9]|1[0-9]|2[0-9]|3[0-1]|[1-9])$/', $dbDate) && checkdate($month, $day, $year))
		{
			return $dbDate;
		}

		return '';
	}

	static function cleanFormInput($str, $maxLength = 0, $strength = 2, $extraAllowed = '')
	{
		$returnString = '';
		$replaceChars = 1;
		$extraAllowed = preg_quote($extraAllowed);
		switch ($strength)
		{
			case 0:
				$replaceChars = 0;
				break;
			case 1:
				//allow letters, numbers & space and some other chars for messages posted
				$legalChars = '%[^0-9a-zA-Z!#\"\'?@$\%;:.\^\[\]{}/*()+=|_&, \-]%';
				break;
			default:
			case 2:
				//allow only letters, numbers & space and chrs found in names and addresses INCLUDING HYPENS, thankyou!
				$legalChars = "%[^0-9a-zA-Z#'.& \-$extraAllowed]%";
				break;
			case 3:
				//allow only letters, numbers & space - eg: zip codes, including Canada..
				$legalChars = "%[^0-9a-zA-Z$extraAllowed ]%";
				break;
			case 4:
				//allow only letters
				$legalChars = "%[^a-zA-Z$extraAllowed ]%";
				break;
			case 5:
				//allow only numbers (they may have - or . in them)
				$legalChars = "%[^0-9\-\.$extraAllowed ]%";
				break;
			case 6:
				//allow only letters, numbers (NO Spaces)
				$legalChars = "%[^0-9a-zA-Z$extraAllowed]%";
				break;
			case 7:
				//allow only letters, numbers and forward slashes (for directory checking (NO Spaces)
				$legalChars = "%[^0-9a-zA-Z/\-\._$extraAllowed]%";
				break;
			case 8:
				//allow comma separated lists
				$legalChars = "%[^0-9,$extraAllowed]%";
				break;
			case 9:
				// AmSouth password chrs -- everything but an @ sign, no spaces
				$legalChars = '%[^0-9a-zA-Z!#\"\'?@$\%;:_.\^\[\]{}/*()+=|&,\-]%';
				break;
			case 10:
				// base64 encoded string (amsouth oldid hash) - see above
				$legalChars = "%[^0-9a-zA-Z/+=$extraAllowed]%";
				break;
			case 11:
				$legalChars = '%[^0-9a-zA-Z_\- \'\.,@:\?!\(\)\$\\\/]%';
				break;
		}

		$returnString = stripslashes(strip_tags($str));
		if ($replaceChars == 1)
		{
			$returnString = preg_replace($legalChars, "", $returnString);
		}
		$returnString = addslashes($returnString);
		if ($maxLength > 0)
		{
			$returnString = substr($returnString, 0, $maxLength);
		}

		return trim($returnString);
	}

	static function removeBadWords($var)
	{
		if (is_array($var))
		{
			foreach ($var as $k => $v)
			{
				$var[$k] = self::removeBadWords($v);
			}
		}
		$search = array('script', 'style');
		$replace = array('');
		$var = str_replace($search, $replace, $var);

		return $var;
	}

	static function hash($data, $algo = 'sha256')
	{
		if (!in_array($algo, hash_algos()))
		{
			$e = new ErrorManager("Invalid algorithm");
			$e->handleError();
		}
		return base64_encode(hash($algo, $data, true));
	}

	static function hashPassword($data, $algo = 'sha256', $salt = '')
	{
		if (!in_array($algo, hash_algos()))
		{
			$e = new ErrorManager("Invalid algorithm");
			$e->handleError();
		}
		if (callingFunction() != 'hashValidate' && $salt != '')
		{
			$e = new ErrorManager("Salt will be automatically generated and should not be passed");
			$e->handleError();
		}
		$salt = $salt == '' ? base64_encode(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM)) : $salt;
		return $algo . ':' . $salt . ':' . base64_encode(hash($algo, $salt . $data, true));
	}

	static function hashValidate($hash, $data)
	{
		$hashData = $algo = $salt = '';
		@list($algo, $salt, $hashData) = explode(':', $hash);
		if (!in_array($algo, hash_algos()) || strlen($salt) == 0 || strlen($hashData) == 0)
		{
			$e = new ErrorManager("Invalid hash provided: $hash");
			$e->handleError(false);
			return false;
		}
		if (Security::hashPassword($data, $algo, $salt) == $hash)
		{
			return true;
		}
		return false;
	}

	static function randSecure($min, $max)
	{
		$range = $max - $min;
		if ($range == 0)
			return $min;
		$log = log($range, 2);
		$bytes = (int) ($log / 8) + 1; // length in bytes
		$bits = (int) $log + 1; // length in bits
		$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
		do
		{
			$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes, $s)));
			$rnd = $rnd & $filter; // discard irrelevant bits
		}
		while ($rnd >= $range);
		return $min + $rnd;
	}

	static function generatePassword($length = 6, $alpha = 't', $numeric = 't', $uppercase = 'f', $extrachars = '')
	{
		$numericlength = $uppercaselength = $alphalength = $extracharslength = 0;
		$passArray = array();

		if ($length < 6)
		{
			return false;
		}

		if ($numeric == 't')
		{
			$numericlength = ceil(0.3 * $length);
		}
		if ($uppercase == 't')
		{
			$uppercaselength = ceil(0.2 * $length);
		}
		if (strlen($extrachars) > 0)
		{
			$extracharslength = ceil(0.2 * $length);
			$numexchars = strlen($extrachars);
		}
		if ($alpha == 't')
		{
			$alphalength = $length - $numericlength - $uppercaselength - $extracharslength;
		}

		if ($alpha == 't')
		{
			$alphachars = 'abcdefghijklmnopqrstuvwxyz';
			for ($i = 0; $i < $alphalength; $i ++)
			{
				$passArray[] = substr($alphachars, Security::randSecure(0, 23), 1);
			}
		}

		if ($numeric == 't')
		{
			$numbers = '1234567890';
			for ($i = 0; $i < $numericlength; $i ++)
			{
				$passArray[] = substr($numbers, Security::randSecure(0, 9), 1);
			}
		}

		if ($uppercase == 't')
		{
			$upperchars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
			for ($i = 0; $i < $uppercaselength; $i ++)
			{
				$passArray[] = substr($upperchars, Security::randSecure(0, 23), 1);
			}
		}

		if (strlen($extrachars) > 0)
		{
			for ($i = 0; $i < $extracharslength; $i ++)
			{
				$passArray[] = substr($extrachars, Security::randSecure(0, $numexchars - 1), 1);
			}
		}

		shuffle($passArray);
		$returnString = implode('', $passArray);

		return $returnString;
	}

}
