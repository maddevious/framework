<?php

class Email
{
	static function sendAlert($to, $subject, $body)
	{
		$lockfile = '/tmp/alertLock';
		$now = time();
		if (!is_file($lockfile) || (is_file($lockfile) && ($now - filemtime($lockfile) > 3600)))
		{
			touch($lockfile);
			mail($to, $subject, $body);
		}
	}
}